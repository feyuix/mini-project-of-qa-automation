<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Register_email</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>d235a4d0-397a-48a9-9d75-2526be25fe36</testSuiteGuid>
   <testCaseLink>
      <guid>d7871c2e-ed7e-4619-ac37-7e3c83e6d2d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <iterationNameVariable>
         <defaultValue>''</defaultValue>
         <description></description>
         <id>d05d3cf0-864c-4e85-9d1f-7139ab6bec37</id>
         <masked>false</masked>
         <name>email_reg</name>
      </iterationNameVariable>
      <testCaseId>Test Cases/Register/Positive case/Register</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>096093f3-1fca-4b94-84a2-7be80afcf541</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataRaw/Data_Reg/Data_email</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>096093f3-1fca-4b94-84a2-7be80afcf541</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>email</value>
         <variableId>d05d3cf0-864c-4e85-9d1f-7139ab6bec37</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
