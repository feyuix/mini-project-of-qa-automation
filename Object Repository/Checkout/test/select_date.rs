<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_date</name>
   <tag></tag>
   <elementGuidId>7e96f704-fa5d-4e66-8b70-2b5a7a83efaf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ExpireMonth</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ExpireMonth']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>5b59cd7c-a8e1-4227-a177-f395d80e645c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ExpireMonth</value>
      <webElementGuid>28c96e2d-418c-41a5-a401-42cb7d5ce1a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ExpireMonth</value>
      <webElementGuid>9a24f638-2809-4e17-9282-8aa49f3d0a19</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>01
02
03
04
05
06
07
08
09
10
11
12
</value>
      <webElementGuid>4bcab481-5359-42bf-bfae-0ec664c7247b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ExpireMonth&quot;)</value>
      <webElementGuid>dfd9185f-55f9-48dd-9895-373a9deaf63c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ExpireMonth']</value>
      <webElementGuid>9453f21e-4198-425d-9a48-3a54c271c017</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-info-load']/div/div/div/table/tbody/tr[4]/td[2]/select</value>
      <webElementGuid>ae7c5ebb-d0b0-454f-8405-9bd8979da21a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Expiration date'])[1]/following::select[1]</value>
      <webElementGuid>22a6c160-79e7-4b6b-9896-e12c31b51614</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Card code'])[1]/preceding::select[2]</value>
      <webElementGuid>3bea388a-c428-4a3a-918e-295d6bb50975</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/select</value>
      <webElementGuid>4033986c-d36c-4d0f-86fd-0d96178f62ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'ExpireMonth' and @name = 'ExpireMonth' and (text() = '01
02
03
04
05
06
07
08
09
10
11
12
' or . = '01
02
03
04
05
06
07
08
09
10
11
12
')]</value>
      <webElementGuid>0e02bdc4-f8d4-497e-87d0-4f0a07c1ff7e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
