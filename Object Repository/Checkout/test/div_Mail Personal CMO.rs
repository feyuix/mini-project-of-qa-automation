<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Mail Personal CMO</name>
   <tag></tag>
   <elementGuidId>987f1f47-aecb-42f1-b694-c5c682cc4046</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.info</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-payment-info-load']/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0b679b3b-8b2d-43c3-8f1a-56ffc204ac27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>info</value>
      <webElementGuid>4a651d15-1498-4b8f-be65-b166fe8f9ee4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            

    
        
            Mail Personal or Business Check, Cashier's Check or money order to:Tricentis GmbH Leonard-Bernstein-Straße 10 1220 Vienna AustriaNotice that if you pay by Personal or Business Check, your order may be held for up to 10 days after we receive your check to allow enough time for the check to clear.  If you want us to ship faster upon receipt of your payment, then we recommend your send a money order or Cashier's check.P.S. You can edit this text from admin panel.
        
    


        </value>
      <webElementGuid>3ac455d5-8465-4fc7-a48b-60093a384a86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-payment-info-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;section payment-info&quot;]/div[@class=&quot;info&quot;]</value>
      <webElementGuid>cb4856e6-25ec-4cb8-866b-5fbefc7c5b9b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-info-load']/div/div/div</value>
      <webElementGuid>7dc443c0-5478-42ad-b7c3-b2fbc2aa9bc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment information'])[1]/following::div[5]</value>
      <webElementGuid>bb704ab3-140a-46a6-83c5-db8dd5606e4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[4]/following::div[6]</value>
      <webElementGuid>7ea1ebd9-fde4-4c89-8b52-3b4739240a41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/div[2]/form/div/div/div/div</value>
      <webElementGuid>7065fb51-8dd8-42e3-9505-d5d9ae26a513</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
            

    
        
            Mail Personal or Business Check, Cashier&quot; , &quot;'&quot; , &quot;s Check or money order to:Tricentis GmbH Leonard-Bernstein-Straße 10 1220 Vienna AustriaNotice that if you pay by Personal or Business Check, your order may be held for up to 10 days after we receive your check to allow enough time for the check to clear.  If you want us to ship faster upon receipt of your payment, then we recommend your send a money order or Cashier&quot; , &quot;'&quot; , &quot;s check.P.S. You can edit this text from admin panel.
        
    


        &quot;) or . = concat(&quot;
            

    
        
            Mail Personal or Business Check, Cashier&quot; , &quot;'&quot; , &quot;s Check or money order to:Tricentis GmbH Leonard-Bernstein-Straße 10 1220 Vienna AustriaNotice that if you pay by Personal or Business Check, your order may be held for up to 10 days after we receive your check to allow enough time for the check to clear.  If you want us to ship faster upon receipt of your payment, then we recommend your send a money order or Cashier&quot; , &quot;'&quot; , &quot;s check.P.S. You can edit this text from admin panel.
        
    


        &quot;))]</value>
      <webElementGuid>ea8e4a67-f0e7-4067-99b3-3b683c8eed10</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
