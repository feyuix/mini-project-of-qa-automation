<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_year</name>
   <tag></tag>
   <elementGuidId>52d66502-d62f-4de8-b6d5-4985cc264a90</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ExpireYear</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ExpireYear']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>8587d6a6-3041-4fbd-a960-3a07f9b6168d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ExpireYear</value>
      <webElementGuid>15ec827a-a383-4443-ba48-e82acd899510</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ExpireYear</value>
      <webElementGuid>cadbb4b9-ca3d-4c58-aa21-e775d3e75fa5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>2024
2025
2026
2027
2028
2029
2030
2031
2032
2033
2034
2035
2036
2037
2038
</value>
      <webElementGuid>ce0b56b0-7137-4b84-83c9-d460ff5d6f49</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ExpireYear&quot;)</value>
      <webElementGuid>e21273ac-3f4d-4dbb-9a1c-5dc6636c78b9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ExpireYear']</value>
      <webElementGuid>0c0faf10-dc7e-4ef2-ba03-09b1635008d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-info-load']/div/div/div/table/tbody/tr[4]/td[2]/select[2]</value>
      <webElementGuid>9177efb3-f523-4e7e-ac1f-51741dff21d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Expiration date'])[1]/following::select[2]</value>
      <webElementGuid>7f239d1f-b81e-44a3-87c8-aa75cd1aa678</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Card code'])[1]/preceding::select[1]</value>
      <webElementGuid>f6394e24-d609-457f-b7ab-ea1d143fcfa4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select[2]</value>
      <webElementGuid>1d736cdf-82d7-46b8-bcde-19ffcf711327</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'ExpireYear' and @name = 'ExpireYear' and (text() = '2024
2025
2026
2027
2028
2029
2030
2031
2032
2033
2034
2035
2036
2037
2038
' or . = '2024
2025
2026
2027
2028
2029
2030
2031
2032
2033
2034
2035
2036
2037
2038
')]</value>
      <webElementGuid>483644ba-f4d6-4db3-8afd-c576caf00efd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
