<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>inp_CCnumber</name>
   <tag></tag>
   <elementGuidId>8c80ded7-f85a-4574-bf4a-b268d1602fdc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#CardNumber</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='CardNumber']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>50a08cd1-4590-491a-b62c-af18a470f7a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>be92facd-a846-4c2f-8cf0-709d8d9c8366</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>CardNumber</value>
      <webElementGuid>54fac39b-a511-4d00-b139-fe6f47b00381</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>22</value>
      <webElementGuid>b6339d96-4a35-4b61-9402-608a1262836f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>CardNumber</value>
      <webElementGuid>22cbefc0-7352-4603-849d-e0f92b8c46a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>a8b98afb-1e6c-46c3-9d9d-b42cbe2f1642</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;CardNumber&quot;)</value>
      <webElementGuid>c0f65a55-43af-40b2-b1a8-ecb54cfdd410</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='CardNumber']</value>
      <webElementGuid>9f050a9e-c8f6-4b14-9322-15cce6ec6528</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-info-load']/div/div/div/table/tbody/tr[3]/td[2]/input</value>
      <webElementGuid>3d8c8bf4-9207-4781-a229-e414945c1e9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[3]/td[2]/input</value>
      <webElementGuid>f5ba87c5-e9df-48e6-a490-70dc7328d792</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'CardNumber' and @name = 'CardNumber' and @type = 'text']</value>
      <webElementGuid>8617e194-8497-40e6-a186-c4a60e54963b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
