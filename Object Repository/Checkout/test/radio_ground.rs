<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>radio_ground</name>
   <tag></tag>
   <elementGuidId>c846b7d3-13e0-4957-ab2d-434d48f89154</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#shippingoption_0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='shippingoption_0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ec8a7dfe-f898-401d-8ef7-456ea8f6ce2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>shippingoption_0</value>
      <webElementGuid>634d5980-b744-42e4-8c67-654601ea1bd5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>efa087e3-49da-4548-8cca-e24d860616c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>shippingoption</value>
      <webElementGuid>c145fd45-b272-4e88-b280-14aad4960c99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Ground___Shipping.FixedRate</value>
      <webElementGuid>7a50d31b-9bb7-468f-83ee-c079c2769c92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checked</name>
      <type>Main</type>
      <value>checked</value>
      <webElementGuid>cdbbea4e-f2ab-4428-babf-4a315ded1c6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;shippingoption_0&quot;)</value>
      <webElementGuid>887baaf0-1a98-4884-9aad-ee07a77e9450</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='shippingoption_0']</value>
      <webElementGuid>c5542477-a6e0-4d70-9a8b-984b6ed06a99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-shipping-method-load']/div/div/ul/li/div/input</value>
      <webElementGuid>7042ee7e-0bae-4545-bf8d-3ef4aa57c558</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div/input</value>
      <webElementGuid>b3c099a5-fa10-41e9-aba4-2e559f43b770</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'shippingoption_0' and @type = 'radio' and @name = 'shippingoption' and @checked = 'checked']</value>
      <webElementGuid>491fdeea-3149-4d8c-82d6-ab34ff1065ab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
