<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_You will pay by COD</name>
   <tag></tag>
   <elementGuidId>c9b6e963-d9d2-4100-ba17-0f83b465abff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>td > p</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-payment-info-load']/div/div/div/table/tbody/tr/td/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>06e061ce-2e51-4676-bc0a-525556fa3c6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>You will pay by COD</value>
      <webElementGuid>7790ea8d-8591-4a55-9932-9269a2f54125</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-payment-info-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;section payment-info&quot;]/div[@class=&quot;info&quot;]/table[1]/tbody[1]/tr[1]/td[1]/p[1]</value>
      <webElementGuid>df343e81-03b9-442a-a31d-52e1086bc591</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-info-load']/div/div/div/table/tbody/tr/td/p</value>
      <webElementGuid>a518b0b0-f562-40eb-a219-8172a8605754</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment information'])[1]/following::p[1]</value>
      <webElementGuid>adbc7be8-c3af-48a4-9cec-13e86b4a712e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[4]/following::p[1]</value>
      <webElementGuid>785ce3fc-b640-4133-a0ba-b6ba713a152b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='«'])[4]/preceding::p[1]</value>
      <webElementGuid>63dcfb0b-beae-48fa-a3cf-f0731230c018</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[5]/preceding::p[2]</value>
      <webElementGuid>ed923871-a202-4726-abb9-fcec2ea8ee45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='You will pay by COD']/parent::*</value>
      <webElementGuid>fc66fd29-ce53-4506-b5e9-7308997f3752</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/p</value>
      <webElementGuid>8671f85e-3336-4e3f-9de4-96df6dd3b03d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'You will pay by COD' or . = 'You will pay by COD')]</value>
      <webElementGuid>0a4f7ac3-d1b3-4a66-9b26-38de084e771a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
