<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>selec_Address</name>
   <tag></tag>
   <elementGuidId>f92df815-6437-400a-aae4-6da27f2b9fb8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#shipping-address-select</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='shipping-address-select']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>80c307f5-bef4-406f-a8f4-11c64f804df6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>shipping_address_id</value>
      <webElementGuid>49414001-2720-48be-9970-9c9c11f18793</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>shipping-address-select</value>
      <webElementGuid>070f0ec9-791a-4649-a996-43d699a30571</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>address-select</value>
      <webElementGuid>ca1a75da-2ef5-4501-bc2f-89c08bd42194</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>Shipping.newAddress(!this.value)</value>
      <webElementGuid>ff739341-4fbd-4d41-af29-af8aa4dad3fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            Febby Nadia, jl.aaa, jakarta, Alabama 28281, United States
                        New Address
                    </value>
      <webElementGuid>886ac757-12a2-44ff-9758-e83cd5faa766</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;shipping-address-select&quot;)</value>
      <webElementGuid>96113160-afcf-4c58-bfb6-f8a3d755e43c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='shipping-address-select']</value>
      <webElementGuid>52d83ffc-b206-4004-88e9-ba3de9350294</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='shipping-addresses-form']/div/div/select</value>
      <webElementGuid>e1e327a9-1938-47a4-899a-170645b4905c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select a shipping address from your address book or enter a new address.'])[1]/following::select[1]</value>
      <webElementGuid>35e60cbb-8f0f-43b2-8f3e-37aa41b9b87a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shipping address'])[1]/following::select[1]</value>
      <webElementGuid>7a0b6b46-1cd1-4681-bc71-a8ce4a4fb7f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='First name:'])[2]/preceding::select[1]</value>
      <webElementGuid>aaae1f57-1eb2-45d5-88bb-f9bacc7efc76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[9]/preceding::select[1]</value>
      <webElementGuid>423ba449-8394-42ef-b3e5-369fcb6f8d8b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/div/div/div/select</value>
      <webElementGuid>2b247a21-8e98-4c04-87e1-f7668cc3d8de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'shipping_address_id' and @id = 'shipping-address-select' and (text() = '
                            Febby Nadia, jl.aaa, jakarta, Alabama 28281, United States
                        New Address
                    ' or . = '
                            Febby Nadia, jl.aaa, jakarta, Alabama 28281, United States
                        New Address
                    ')]</value>
      <webElementGuid>1d57e3a0-a09f-4127-9ae6-6c3bdca944d1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
