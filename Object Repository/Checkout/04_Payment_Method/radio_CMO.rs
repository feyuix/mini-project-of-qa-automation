<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>radio_CMO</name>
   <tag></tag>
   <elementGuidId>07de4485-2825-4789-be10-cd51c0812030</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#paymentmethod_1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='paymentmethod_1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>b9ce879e-26a8-4305-8179-e00575ee7839</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>paymentmethod_1</value>
      <webElementGuid>6ade4eef-7241-4a12-9c85-7c3aaaaed8f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>94b7c6b0-d574-4429-8c3b-fc8133870c31</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>paymentmethod</value>
      <webElementGuid>3ebd017b-b443-4c13-986c-e31c31f3fad3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Payments.CheckMoneyOrder</value>
      <webElementGuid>f1e4c0b5-ca1b-4475-8915-e1b7bc09cbab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;paymentmethod_1&quot;)</value>
      <webElementGuid>404ee23c-6e5c-4f96-8d44-dc89149cc131</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='paymentmethod_1']</value>
      <webElementGuid>c859313f-9820-43f7-a1f1-a85a4ff73064</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-method-load']/div/div/ul/li[2]/div/div[2]/input</value>
      <webElementGuid>410172dc-31dc-4fc2-975a-977b4dc9cea8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/div/div[2]/input</value>
      <webElementGuid>7b849822-784c-4d57-83ae-c8133d590959</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'paymentmethod_1' and @type = 'radio' and @name = 'paymentmethod']</value>
      <webElementGuid>6f992d95-0c18-466d-b51f-03ce675dde94</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
