<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>radio_CC</name>
   <tag></tag>
   <elementGuidId>bfadb88e-228d-4558-970c-93218a4b3a77</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#paymentmethod_2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='paymentmethod_2']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>03b05bb8-ca8c-4a59-ad0f-3162002414ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>paymentmethod_2</value>
      <webElementGuid>545c7097-49bb-4c83-9379-e0460b84e60a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>3c25a948-042e-4532-9014-fc8812799989</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>paymentmethod</value>
      <webElementGuid>1a9dad68-cb5b-4fdf-b76e-af1b17198e0e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Payments.Manual</value>
      <webElementGuid>f4d8ca9c-8cbd-48b0-b34a-848abc7df573</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;paymentmethod_2&quot;)</value>
      <webElementGuid>1fbfc352-2745-436e-8f3e-51d855c3dcb3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='paymentmethod_2']</value>
      <webElementGuid>506ed630-c5f8-4d77-8db0-6e19f3d49de6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-method-load']/div/div/ul/li[3]/div/div[2]/input</value>
      <webElementGuid>6659db1f-94dc-4bf4-9f40-488d45150b3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/div/div[2]/input</value>
      <webElementGuid>6e7e389e-9f47-44e3-8bfe-e90487b43f3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'paymentmethod_2' and @type = 'radio' and @name = 'paymentmethod']</value>
      <webElementGuid>e69d83e8-9e6f-4cf2-83ec-be9b849b0bd0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
