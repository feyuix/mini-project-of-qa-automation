<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Purchase Order</name>
   <tag></tag>
   <elementGuidId>801d6ccf-7936-4702-aa3a-cc0024154784</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.payment-method</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul/li[11]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>804f0b52-026e-464a-8b5b-14c94172902c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>payment-method</value>
      <webElementGuid>f5a13ed8-42ab-429f-a390-196d9e49597a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Purchase Order
                </value>
      <webElementGuid>578a8e15-ba0c-4d6e-bc5a-7497cc33850b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-confirm-order-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;order-summary-body&quot;]/div[@class=&quot;order-summary-content&quot;]/div[@class=&quot;order-review-data&quot;]/ul[@class=&quot;billing-info&quot;]/li[@class=&quot;payment-method&quot;]</value>
      <webElementGuid>219ee012-2feb-4668-a942-759d8d843f02</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul/li[11]</value>
      <webElementGuid>27ce4063-b4e5-46ea-9a41-3271a945986e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment Method'])[1]/following::li[1]</value>
      <webElementGuid>4e26fe66-0f8b-434f-b381-922bbd8d649a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='United States'])[3]/following::li[2]</value>
      <webElementGuid>54e6f943-f594-456d-a22e-71698efb77d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shipping Address'])[1]/preceding::li[1]</value>
      <webElementGuid>709343a6-d569-4fe7-bdef-b3b3f547dfe8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Febby Nadia'])[2]/preceding::li[2]</value>
      <webElementGuid>c6759ca2-a82e-4192-97f6-b7e4e7c1bdfc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[11]</value>
      <webElementGuid>49a09511-36aa-4a3e-95ec-6f42471ad133</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                        Purchase Order
                ' or . = '
                        Purchase Order
                ')]</value>
      <webElementGuid>98869d46-a495-4702-9cc9-af020463a5f9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
