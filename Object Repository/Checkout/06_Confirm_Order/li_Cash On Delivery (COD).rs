<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Cash On Delivery (COD)</name>
   <tag></tag>
   <elementGuidId>6e544828-f515-4484-b704-37d6db0e4376</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.payment-method</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul/li[11]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>7752bec5-e0c9-4715-a272-4d2d2753779f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>payment-method</value>
      <webElementGuid>7de5a7a9-e9d6-4e9c-ac54-0b2c6352b1f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Cash On Delivery (COD)
                </value>
      <webElementGuid>75764945-28a6-455a-bc8f-2a10ea675d66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-confirm-order-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;order-summary-body&quot;]/div[@class=&quot;order-summary-content&quot;]/div[@class=&quot;order-review-data&quot;]/ul[@class=&quot;billing-info&quot;]/li[@class=&quot;payment-method&quot;]</value>
      <webElementGuid>f6f7fe08-70a1-4a43-87fd-c1937e40d7d9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul/li[11]</value>
      <webElementGuid>d086d7be-9f23-47a8-82bd-87201d9461b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment Method'])[1]/following::li[1]</value>
      <webElementGuid>925e2c6f-e9b3-4d40-8487-0869f3f17c26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='United States'])[3]/following::li[2]</value>
      <webElementGuid>10a9bfc6-dbb7-44f2-8ea1-c0939c1789d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shipping Address'])[1]/preceding::li[1]</value>
      <webElementGuid>eb6d209d-e60b-4b54-9244-598d6a9ce54d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Febby Nadia'])[2]/preceding::li[2]</value>
      <webElementGuid>4020df6a-5fa4-4f74-9bfe-fee5e37a7dd9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Cash On Delivery (COD)']/parent::*</value>
      <webElementGuid>35211443-0b5b-49df-bf21-111e84e661fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[11]</value>
      <webElementGuid>bcb8967d-d378-4674-bd2c-fb20aa158358</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                        Cash On Delivery (COD)
                ' or . = '
                        Cash On Delivery (COD)
                ')]</value>
      <webElementGuid>ab8fc1f9-0e6c-40a4-8ee6-930bace216e2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
