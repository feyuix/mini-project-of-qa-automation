<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Credit Card</name>
   <tag></tag>
   <elementGuidId>0c12fe3a-9e2b-433a-8851-e0bc1ee5c5c8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.payment-method</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul/li[11]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>798739a3-177d-4d07-b803-05b8dd373a06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>payment-method</value>
      <webElementGuid>0b40e577-89e2-458a-8d3e-1fd538e3ac0f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Credit Card
                </value>
      <webElementGuid>4c72e306-5ef4-4a53-baab-6d72958cd0fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-confirm-order-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;order-summary-body&quot;]/div[@class=&quot;order-summary-content&quot;]/div[@class=&quot;order-review-data&quot;]/ul[@class=&quot;billing-info&quot;]/li[@class=&quot;payment-method&quot;]</value>
      <webElementGuid>9297c82d-43b5-46c9-a551-9dea1013c957</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-confirm-order-load']/div/div[2]/div/div/ul/li[11]</value>
      <webElementGuid>5cd6cd5d-8273-4999-b9fc-fac51624cd6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment Method'])[1]/following::li[1]</value>
      <webElementGuid>6cae8ef8-63d8-4445-be87-12f925ac87b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='United States'])[3]/following::li[2]</value>
      <webElementGuid>88df89d7-5f45-43f0-840c-233d66a782ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shipping Address'])[1]/preceding::li[1]</value>
      <webElementGuid>7e6259b7-8398-45f5-91c2-bc21d22ee969</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Febby Nadia'])[2]/preceding::li[2]</value>
      <webElementGuid>7da99fa9-8388-4c12-8d39-dece8c987848</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[11]</value>
      <webElementGuid>cbc2c017-285e-42d0-9d7c-0bd5e2138a57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                        Credit Card
                ' or . = '
                        Credit Card
                ')]</value>
      <webElementGuid>d5b572ca-1d74-44bd-acfd-2dbb7534dbe2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
