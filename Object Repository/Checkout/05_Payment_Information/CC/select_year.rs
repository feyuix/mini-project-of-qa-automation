<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_year</name>
   <tag></tag>
   <elementGuidId>9417f107-74dd-4eb5-bfb3-43af513edab5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ExpireYear</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ExpireYear']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>b2447664-f343-4336-a893-9f9ec2dac063</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ExpireYear</value>
      <webElementGuid>7d72eda6-0590-4056-9153-6d2ebed06dfa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ExpireYear</value>
      <webElementGuid>1afa7512-aabf-4a73-a747-cae93be36075</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>valid</value>
      <webElementGuid>4a979b2b-1adf-4fdd-b2e9-7425b9255e32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>2024
2025
2026
2027
2028
2029
2030
2031
2032
2033
2034
2035
2036
2037
2038
</value>
      <webElementGuid>f10fde14-9626-481a-a54c-18d1da9c7b8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ExpireYear&quot;)</value>
      <webElementGuid>896b40ea-46d7-46a9-8047-6722fe6dfe76</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ExpireYear']</value>
      <webElementGuid>e0e7e298-00c9-4763-b4c4-874d51b648dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-info-load']/div/div/div/table/tbody/tr[4]/td[2]/select[2]</value>
      <webElementGuid>572c4494-75fa-4338-82a1-fb5b2cc0546e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Expiration date'])[1]/following::select[2]</value>
      <webElementGuid>905f49fe-ddf7-419c-a390-aa38a5213a2a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Card code'])[1]/preceding::select[1]</value>
      <webElementGuid>ef417890-ef04-4c17-a833-b1c3ca530d27</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select[2]</value>
      <webElementGuid>ac8e7cac-999a-440a-ac1c-73b6102f0100</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'ExpireYear' and @name = 'ExpireYear' and (text() = '2024
2025
2026
2027
2028
2029
2030
2031
2032
2033
2034
2035
2036
2037
2038
' or . = '2024
2025
2026
2027
2028
2029
2030
2031
2032
2033
2034
2035
2036
2037
2038
')]</value>
      <webElementGuid>75d38a0e-6bab-468b-87ad-380c10a0f2fb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
