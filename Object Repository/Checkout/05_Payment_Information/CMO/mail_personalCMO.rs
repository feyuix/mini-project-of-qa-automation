<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>mail_personalCMO</name>
   <tag></tag>
   <elementGuidId>7887f568-4941-4f5a-8ed0-3101224faef4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>td</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='checkout-payment-info-load']/div/div/div/table/tbody/tr/td</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>51c65696-4f63-4374-a704-2d414c9104bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Mail Personal or Business Check, Cashier's Check or money order to:Tricentis GmbH Leonard-Bernstein-Straße 10 1220 Vienna AustriaNotice that if you pay by Personal or Business Check, your order may be held for up to 10 days after we receive your check to allow enough time for the check to clear.  If you want us to ship faster upon receipt of your payment, then we recommend your send a money order or Cashier's check.P.S. You can edit this text from admin panel.
        </value>
      <webElementGuid>bcdc581d-a9bb-459f-8b52-4ebef8be2363</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-payment-info-load&quot;)/div[@class=&quot;checkout-data&quot;]/div[@class=&quot;section payment-info&quot;]/div[@class=&quot;info&quot;]/table[1]/tbody[1]/tr[1]/td[1]</value>
      <webElementGuid>e3392b9d-c934-4074-b3e7-79ebc091b291</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-info-load']/div/div/div/table/tbody/tr/td</value>
      <webElementGuid>b5788cdb-a627-4d41-b146-5f0a2b3510dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment information'])[1]/following::td[1]</value>
      <webElementGuid>00a9ed45-9f33-4e24-afa8-606fbeda9642</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading next step...'])[4]/following::td[1]</value>
      <webElementGuid>13cbfb27-b1e5-468a-94b5-e00022d5e750</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td</value>
      <webElementGuid>8ea13fa0-37be-4703-bf5e-e917c7ad376a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = concat(&quot;
            Mail Personal or Business Check, Cashier&quot; , &quot;'&quot; , &quot;s Check or money order to:Tricentis GmbH Leonard-Bernstein-Straße 10 1220 Vienna AustriaNotice that if you pay by Personal or Business Check, your order may be held for up to 10 days after we receive your check to allow enough time for the check to clear.  If you want us to ship faster upon receipt of your payment, then we recommend your send a money order or Cashier&quot; , &quot;'&quot; , &quot;s check.P.S. You can edit this text from admin panel.
        &quot;) or . = concat(&quot;
            Mail Personal or Business Check, Cashier&quot; , &quot;'&quot; , &quot;s Check or money order to:Tricentis GmbH Leonard-Bernstein-Straße 10 1220 Vienna AustriaNotice that if you pay by Personal or Business Check, your order may be held for up to 10 days after we receive your check to allow enough time for the check to clear.  If you want us to ship faster upon receipt of your payment, then we recommend your send a money order or Cashier&quot; , &quot;'&quot; , &quot;s check.P.S. You can edit this text from admin panel.
        &quot;))]</value>
      <webElementGuid>d6fd108c-090a-4963-93a7-380f2077aba9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
