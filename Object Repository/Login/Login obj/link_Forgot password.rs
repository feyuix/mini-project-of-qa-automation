<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link_Forgot password</name>
   <tag></tag>
   <elementGuidId>5b303b53-729b-4338-b028-5105e73a0e9a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.forgot-password > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Forgot password?')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>7bf6252c-07ef-40d9-9c93-6c25ae3d1ab6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/passwordrecovery</value>
      <webElementGuid>f1f61c99-a13b-4f41-b4ce-2db76cb73930</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Forgot password?</value>
      <webElementGuid>bc7df611-b5d4-417f-9c4e-1329d2cac97f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page login-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;customer-blocks&quot;]/div[@class=&quot;returning-wrapper&quot;]/div[@class=&quot;form-fields&quot;]/form[1]/div[@class=&quot;inputs reversed&quot;]/span[@class=&quot;forgot-password&quot;]/a[1]</value>
      <webElementGuid>318631bf-761f-4743-9893-d5a3d4c35521</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Forgot password?')]</value>
      <webElementGuid>404f4578-1a97-43bd-a46e-0c9abf97b169</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remember me?'])[1]/following::a[1]</value>
      <webElementGuid>7cb077ad-1502-48fc-a31a-6b5fc06e95b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password:'])[1]/following::a[1]</value>
      <webElementGuid>8f55f35c-2597-4313-bb72-c0a752f4c87c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About login / registration'])[1]/preceding::a[1]</value>
      <webElementGuid>9a53af8e-eae9-4443-a8ee-8c3c74011a80</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::a[1]</value>
      <webElementGuid>2a721776-492b-407a-82c9-0e9e29e19b5d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Forgot password?']/parent::*</value>
      <webElementGuid>04120139-de82-4678-99c8-679b5c72a039</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/passwordrecovery')]</value>
      <webElementGuid>4d0e4740-ff4b-4872-b4ea-bd3499c8ebbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/a</value>
      <webElementGuid>8560b665-9053-4d77-afd5-700c44c3b160</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/passwordrecovery' and (text() = 'Forgot password?' or . = 'Forgot password?')]</value>
      <webElementGuid>e5410b7c-dbfc-4d23-a817-5fad388af569</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
