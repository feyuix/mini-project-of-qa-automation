<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Adv_Category</name>
   <tag></tag>
   <elementGuidId>3bdf91ed-391a-4167-992d-24a1b8c83cbf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='Cid']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#Cid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>a9f249ec-3a1d-479d-a758-fb37c88275a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>bfd1d02a-8893-43c7-a418-9ea8d2c8558c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-number</name>
      <type>Main</type>
      <value>The field Category must be a number.</value>
      <webElementGuid>dbe19138-0289-41f4-8c6e-22411395a46a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Cid</value>
      <webElementGuid>2de830f9-4d75-4b3d-90ac-e2a1d1e096b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Cid</value>
      <webElementGuid>ff7e3f96-b4c8-4f98-bd17-fc26874c2621</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>valid</value>
      <webElementGuid>fdc94bf1-5b46-4fe3-aaba-acfba61b911e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards
</value>
      <webElementGuid>cc73f624-131a-4e01-a297-d7ca9f4b10fd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Cid&quot;)</value>
      <webElementGuid>3eac53c7-2a0e-43b7-8f5d-3cb16b5929f6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='Cid']</value>
      <webElementGuid>029e7c34-8eb1-4c02-80c2-c6981db482f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='advanced-search-block']/div/select</value>
      <webElementGuid>b34cb4a5-20fa-4cd2-902e-2ff32111f2a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Category:'])[1]/following::select[1]</value>
      <webElementGuid>2ea3722c-1cd3-448a-a8c8-ed6d0258cef8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Advanced search'])[1]/following::select[1]</value>
      <webElementGuid>b63e8dbd-d486-4522-b07d-7399da7c19a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automatically search sub categories'])[1]/preceding::select[1]</value>
      <webElementGuid>18222e0a-76d6-4b6e-9740-9c42265434f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manufacturer:'])[1]/preceding::select[1]</value>
      <webElementGuid>82e06b09-6e78-452d-a63e-b9c17f4800b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>905a2ab6-fee1-4406-971d-e89aa2eca86c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'Cid' and @name = 'Cid' and (text() = 'All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards
' or . = 'All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards
')]</value>
      <webElementGuid>0d507b92-c87b-45c0-9402-1753e344acb5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
