<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Search keyword                                                                                                                                    Advanced search</name>
   <tag></tag>
   <elementGuidId>dab9458e-b0d1-45b7-9681-d7755ad66914</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[1]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.search-input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>09744e7c-f67f-42c9-9821-0fab8bd79e6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>search-input</value>
      <webElementGuid>7d6d9026-5687-4192-87a7-95b7e1fa4819</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                    
                        Search keyword:
                        
                    
                    
                    
                        
                        Advanced search
                    
                
                
                        
                            Category:
                            All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards

                        
                        
                            
                            Automatically search sub categories
                        
                                            
                            Manufacturer:
                            All
Tricentis

                        
                    
                        Price range:
                        
                            From
                            
                            to
                            
                        
                    
                    
                        
                        Search In product descriptions
                    
                    
                
                
                    
                
        </value>
      <webElementGuid>32678cdb-c37a-4430-9c2b-827d88fbbf2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page search-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;search-input&quot;]</value>
      <webElementGuid>8ff193e1-6114-426a-9dfc-1a24dfc6ae78</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[1]/following::div[2]</value>
      <webElementGuid>9c53886d-3641-4696-9ebd-b7fb8f201031</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[6]</value>
      <webElementGuid>f408a656-8fec-4076-8353-d63c28282fc6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div</value>
      <webElementGuid>b3bf1280-b064-4e6f-afae-49dc8be4337b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                
                    
                        Search keyword:
                        
                    
                    
                    
                        
                        Advanced search
                    
                
                
                        
                            Category:
                            All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards

                        
                        
                            
                            Automatically search sub categories
                        
                                            
                            Manufacturer:
                            All
Tricentis

                        
                    
                        Price range:
                        
                            From
                            
                            to
                            
                        
                    
                    
                        
                        Search In product descriptions
                    
                    
                
                
                    
                
        ' or . = '
                
                    
                        Search keyword:
                        
                    
                    
                    
                        
                        Advanced search
                    
                
                
                        
                            Category:
                            All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards

                        
                        
                            
                            Automatically search sub categories
                        
                                            
                            Manufacturer:
                            All
Tricentis

                        
                    
                        Price range:
                        
                            From
                            
                            to
                            
                        
                    
                    
                        
                        Search In product descriptions
                    
                    
                
                
                    
                
        ')]</value>
      <webElementGuid>64632827-78ed-4ec7-99a0-a6aa29c2eb61</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
