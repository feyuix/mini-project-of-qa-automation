<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>inp_Adv_PriceFrom</name>
   <tag></tag>
   <elementGuidId>65cd8579-e63a-4eac-be6a-78eade6dbd66</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='Pf']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#Pf</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>52c66982-17ab-4a6f-a96b-aaf86a409001</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>price-from</value>
      <webElementGuid>3004ca97-50ad-4d17-ae24-923a0ece9a8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Pf</value>
      <webElementGuid>1de7983d-6eb1-4639-9ca6-a671bbced139</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Pf</value>
      <webElementGuid>fe9a7914-abb1-459a-9a81-94abd3aff6e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>9a8d6bba-f692-44d4-b255-f1555a75a481</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Pf&quot;)</value>
      <webElementGuid>d6085192-480c-482c-a493-35f5e0ba794b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='Pf']</value>
      <webElementGuid>b0c98b2b-34f1-4800-9f0e-3799315fe449</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='advanced-search-block']/div[4]/span/input</value>
      <webElementGuid>8d77141c-78e5-4c89-9aae-16a1e048b8e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/input</value>
      <webElementGuid>063f6994-964b-40be-ae9f-cd99914daf8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'Pf' and @name = 'Pf' and @type = 'text']</value>
      <webElementGuid>a05607b0-2854-445e-ac85-8d590ad98035</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
