<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_product_view</name>
   <tag></tag>
   <elementGuidId>deb0701e-e8ee-414c-ad4a-be3c8ef13d6a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.product-grid</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c0946199-26c6-4203-9b03-e575dfde3644</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-grid</value>
      <webElementGuid>af8448e3-2120-4070-a1db-d9a276c83e3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                                

    
        
            
        
    
    
        
            Fiction
        
            
                
                    
                    
                
            
        
            Bestselling fiction
        
        
            
                    35.00
                24.00
            
            
                
                    
            
            
        
    


                            
                            
                                

    
        
            
        
    
    
        
            Fiction EX
        
            
                
                    
                    
                
            
        
            Bestselling fiction
        
        
            
                    35.00
                24.00
            
            
                
            
            
        
    


                            
                    </value>
      <webElementGuid>5df62539-126d-4e13-bee4-d0742f3e3eab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page search-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;search-results&quot;]/div[@class=&quot;product-grid&quot;]</value>
      <webElementGuid>cb918595-2bec-4da3-a6fb-a35dbedbeef8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[2]</value>
      <webElementGuid>e7e01bd4-0f2b-4dec-bac5-405fac846332</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::div[2]</value>
      <webElementGuid>361da736-cace-4da5-973a-39e32e6e00ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div</value>
      <webElementGuid>ce7a9ec1-4e17-4b21-b1c2-370f212653bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            
                                

    
        
            
        
    
    
        
            Fiction
        
            
                
                    
                    
                
            
        
            Bestselling fiction
        
        
            
                    35.00
                24.00
            
            
                
                    
            
            
        
    


                            
                            
                                

    
        
            
        
    
    
        
            Fiction EX
        
            
                
                    
                    
                
            
        
            Bestselling fiction
        
        
            
                    35.00
                24.00
            
            
                
            
            
        
    


                            
                    ' or . = '
                            
                                

    
        
            
        
    
    
        
            Fiction
        
            
                
                    
                    
                
            
        
            Bestselling fiction
        
        
            
                    35.00
                24.00
            
            
                
                    
            
            
        
    


                            
                            
                                

    
        
            
        
    
    
        
            Fiction EX
        
            
                
                    
                    
                
            
        
            Bestselling fiction
        
        
            
                    35.00
                24.00
            
            
                
            
            
        
    


                            
                    ')]</value>
      <webElementGuid>4d0aa121-1ffd-41bb-907b-d96436f0e436</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
