<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_The password should have at least 6 characters</name>
   <tag></tag>
   <elementGuidId>e74a4bf6-5da2-432b-899b-e4015759339b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[4]/following::span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.field-validation-error > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>8fe0e718-a2ad-4215-a339-f287e8bf1562</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>Password</value>
      <webElementGuid>d104d58b-8d6d-4f02-9d5f-fd09044aa540</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>The password should have at least 6 characters.</value>
      <webElementGuid>113b31d4-ba80-44fe-a677-3b342d66cdaf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/form[1]/div[@class=&quot;page registration-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;fieldset&quot;]/div[@class=&quot;form-fields&quot;]/div[@class=&quot;inputs&quot;]/span[@class=&quot;field-validation-error&quot;]/span[1]</value>
      <webElementGuid>48222bd4-25de-45ef-b07f-3458ad900b58</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[4]/following::span[2]</value>
      <webElementGuid>f20f5c7e-8b44-40eb-baf7-1de8de87d071</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password:'])[1]/following::span[3]</value>
      <webElementGuid>bce87980-ff97-4e8f-b1de-b5feb621ff25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm password:'])[1]/preceding::span[1]</value>
      <webElementGuid>be85c772-7915-4102-be7a-20a09bdb4f85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[5]/preceding::span[1]</value>
      <webElementGuid>2b2b2ec8-1c29-4c2c-b0e7-07505f34736f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='The password should have at least 6 characters.']/parent::*</value>
      <webElementGuid>453def20-8617-4a54-b379-636340881398</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/span</value>
      <webElementGuid>0e85e30e-dba8-4f65-873b-441dc62c7241</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'The password should have at least 6 characters.' or . = 'The password should have at least 6 characters.')]</value>
      <webElementGuid>52d85027-8037-4709-8eef-a6b8aed1206d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
