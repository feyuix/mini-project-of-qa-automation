<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>inp_Gender_Female</name>
   <tag></tag>
   <elementGuidId>ffb09f68-c991-435b-8c41-d4e69f944541</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='gender-female']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#gender-female</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>40bebadd-ebee-4ea3-b9a5-95be60b074af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>gender-female</value>
      <webElementGuid>bae32531-87bb-46f4-83e0-c6d9d1fa3ca0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Gender</value>
      <webElementGuid>b2650ce6-af3f-49a3-b68a-814922d68f3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>c4b4a806-1ea5-4fb5-88a9-258757f296c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>F</value>
      <webElementGuid>fb2155d7-af6a-473d-9424-96ca2f052d68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gender-female&quot;)</value>
      <webElementGuid>e46ac064-c0f2-438b-bfd1-65779a7190f1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='gender-female']</value>
      <webElementGuid>59794cdf-85ff-4ce0-a670-2a9b789f99d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[2]/div/div[2]/input</value>
      <webElementGuid>9087bd7b-986d-4641-8e53-259fbbed49d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'gender-female' and @name = 'Gender' and @type = 'radio']</value>
      <webElementGuid>74d6db24-622e-415e-82a8-9149debe01de</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
