<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Your registration completed</name>
   <tag></tag>
   <elementGuidId>3a5ea935-3c35-4745-bdaf-5ffee56f8db1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Register'])[1]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.result</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e32d2b39-c1cd-442a-b749-a245935cc15f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>result</value>
      <webElementGuid>bda0dbcc-7ff8-41af-8366-7c68b73401e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Your registration completed
        </value>
      <webElementGuid>1c104b3d-66b8-4ffa-8646-6bef1222cad1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page registration-result-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;result&quot;]</value>
      <webElementGuid>96c7dafa-33cc-4463-9b2c-441268649e72</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Register'])[1]/following::div[2]</value>
      <webElementGuid>c980e8c5-ad0a-4c71-8519-8b91b1eb1e4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::div[6]</value>
      <webElementGuid>8c7d075a-e1f8-41c6-9abb-7ce9f5322bf7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::div[2]</value>
      <webElementGuid>beb4981a-0d7c-4c11-928e-20cf1bcf0260</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sitemap'])[1]/preceding::div[2]</value>
      <webElementGuid>d6a3ed4f-917f-4262-bac4-2f0fa3f59320</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Your registration completed']/parent::*</value>
      <webElementGuid>96083185-a7f0-4236-925e-1a07c0e34355</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div</value>
      <webElementGuid>8f8b47b6-ce57-4c19-a6e3-59261f9c3b7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            Your registration completed
        ' or . = '
            Your registration completed
        ')]</value>
      <webElementGuid>69155ade-787a-41ac-a9dc-8a5a3823cf20</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
