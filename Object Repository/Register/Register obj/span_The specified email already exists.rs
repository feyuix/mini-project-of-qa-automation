<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_The specified email already exists</name>
   <tag></tag>
   <elementGuidId>d7227a42-e0ce-4d60-818f-23c23114ff50</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Register'])[2]/following::li[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.validation-summary-errors > ul > li</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>c6a2f353-b638-46f2-8c49-9d164f321a4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>The specified email already exists</value>
      <webElementGuid>a008f568-3813-433c-86c7-9237dcfab01d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/form[1]/div[@class=&quot;page registration-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;message-error&quot;]/div[@class=&quot;validation-summary-errors&quot;]/ul[1]/li[1]</value>
      <webElementGuid>afb99564-2c6a-43e4-bda7-7d2ba9926c68</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Register'])[2]/following::li[1]</value>
      <webElementGuid>bd2c4d68-264f-4f9e-ae0b-01f98583aea0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::li[1]</value>
      <webElementGuid>167a92b5-a702-4e65-9422-35b8e750921b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your Personal Details'])[1]/preceding::li[1]</value>
      <webElementGuid>613afa4b-78b6-4b98-8709-bfb377e3a5bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gender:'])[1]/preceding::li[1]</value>
      <webElementGuid>f9a9caa1-a93b-45ca-87fa-a4f8fcf8f69c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='The specified email already exists']/parent::*</value>
      <webElementGuid>256e63ce-6dc8-41b8-bcd3-751aa336f447</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div[2]/div/div/ul/li</value>
      <webElementGuid>0ccf2346-52b9-4b2a-924e-644637cd2cd8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'The specified email already exists' or . = 'The specified email already exists')]</value>
      <webElementGuid>2c62fcf8-c6fa-4a0c-a8b7-07ad1c8ce405</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
