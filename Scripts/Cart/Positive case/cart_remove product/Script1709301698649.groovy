import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login/reusable login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Cart/cart_add from product list/inp_Build it_button-2 product-box-add-to-cart-button'))

WebUI.setText(findTestObject('Cart/cart_add from prod detail/inp_Qty_addtocart'), '1')

WebUI.click(findTestObject('Cart/cart_add from prod detail/btn_detail_prod_add-to-cart'))

WebUI.verifyElementVisible(findTestObject('Cart/cart_add from prod detail/p_The product has been added to your shopping cart'))

WebUI.click(findTestObject('Cart/a_Shopping cart'))

WebUI.verifyElementVisible(findTestObject('Cart/cart_page/input_Total_updatecart'))

WebUI.click(findTestObject('Cart/cart_page/input_Remove_removefromcart'))

WebUI.click(findTestObject('Cart/cart_page/input_Total_updatecart'))

WebUI.closeBrowser()

