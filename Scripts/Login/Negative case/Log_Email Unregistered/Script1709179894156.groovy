import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demowebshop.tricentis.com/register')

WebUI.click(findTestObject('Login/Login nav/a_Log in'))

WebUI.setText(findTestObject('Login/Login obj/inp_Log_Email'), 'lilili.pnm@gmail.com')

WebUI.setText(findTestObject('Login/Login obj/inp_Log_Password'), 'Nadia112233')

WebUI.click(findTestObject('Login/Login obj/btn_login'))

WebUI.verifyElementVisible(findTestObject('Login/Login obj/span_Login was unsuccessful. Please correct the errors and try again'))

WebUI.verifyElementVisible(findTestObject('Login/Login obj/li_No customer account found'))

WebUI.closeBrowser(FailureHandling.STOP_ON_FAILURE)

