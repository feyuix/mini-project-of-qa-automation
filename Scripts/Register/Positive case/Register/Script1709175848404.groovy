import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demowebshop.tricentis.com/')

WebUI.click(findTestObject('Register/Register nav/a_Register'))

WebUI.click(findTestObject('Register/Register obj/inp_Gender_Male'))

WebUI.setText(findTestObject('Register/Register obj/inp_First name_FirstName'), 'Febby')

WebUI.setText(findTestObject('Register/Register obj/inp_Last name_LastName'), 'Nadia')

WebUI.setText(findTestObject('Register/Register obj/inp_Email_Email'), email_reg)

WebUI.setText(findTestObject('Register/Register obj/inp_Password_Password'), 'Nadia112233')

WebUI.setText(findTestObject('Register/Register obj/inp_Confirm password_ConfirmPassword'), 'Nadia112233')

WebUI.click(findTestObject('Register/Register obj/btn__register-button'))

WebUI.closeBrowser()

