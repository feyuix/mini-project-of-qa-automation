import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demowebshop.tricentis.com/')

WebUI.setText(findTestObject('Search/search_bar/inp_search-box'), 'books')

WebUI.click(findTestObject('Search/search_bar/btn_search-box'))

WebUI.click(findTestObject('Search/search_advance/chk_Adv_search'))

WebUI.selectOptionByValue(findTestObject('Search/search_advance/select_Adv_Category'), '3', true)

WebUI.click(findTestObject('Search/search_advance/chk_Adv_auto search sub categories'))

WebUI.selectOptionByValue(findTestObject('Search/search_advance/select_Adv_Manufacturer'), '0', true)

WebUI.setText(findTestObject('Search/search_advance/inp_Adv_PriceFrom'), '500')

WebUI.setText(findTestObject('Search/search_advance/inp_Adv_PriceTo'), '1200')

WebUI.click(findTestObject('Search/search_advance/chk_Adv_search prod desc'))

WebUI.click(findTestObject('Search/search_advance/btn_Adv_search btn'))

WebUI.verifyElementVisible(findTestObject('Search/search_advance/strong_No products were found that matched your criteria'))

WebUI.closeBrowser()

